function findMaxIncreasingSequence() {
  const str = jsConsole.read('#str');
  const array = str.replace(/\s+/g,'').split(',').map(function (item) {
    return parseInt(item)
  });
  jsConsole.writeLine(`The input sequence is: <br> ${array.join(' ')} <br>`);
  let result = {
    data: [array[0]],
    lastIndex: 0
  };
  let check = {
    data: [],
    lastIndex: 0,
  };
  for (let i = 1; i < array.length; i++) {
    if (array[i] === array[i - 1] + 1) {
      if (array[i] === result.data[result.data.length - 1] + 1 && result.lastIndex === i - 1) {
        result.data.push(array[i]);
        result.lastIndex = i;
      } else if (array[i] === check.data[check.data.length - 1] + 1 && check.lastIndex === i - 1) {
        check.data.push(array[i]);
        check.lastIndex = i;
        if (result.data.length < check.data.length) {
          result = {
            ...check
          };
          check = {
            data: [],
            lastIndex: 0,
          };
        }
      }
    } else {
      check = {
        data: [array[i]],
        lastIndex: i,
      }

    }

  }
  jsConsole.writeLine(`The maximum increasing sequence is: <br> ${result.data.join(' ')}<br>`);
};