function selectionSort () {
  const str = jsConsole.read('#str');
  const array = str.replace(/\s+/g,'').split(',').map(function (item) {
    return parseInt(item)
  });
  jsConsole.writeLine(`The input sequence is: <br> ${array.join(' ')} <br>`);
  let len = array.length;
  for (let i = 0; i < len; i++) {
    let min = i;
    for (let j = i + 1; j < len; j++) {
      if (array[min] > array[j]) {
        min = j;
      }
    }
    if (min !== i) {
      let tmp = array[i];
      array[i] = array[min];
      array[min] = tmp;
    }
  }
  let result = '';
  for (let index = 0; index < array.length; index ++) {
    result = `${result} ${array[index]}`
  }
  jsConsole.writeLine(`Result: <br> ${result}`)

}