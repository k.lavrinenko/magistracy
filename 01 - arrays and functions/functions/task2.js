function reverseNumber() {
  const str = jsConsole.readInteger('#str');
  if (typeof str !== 'number') {
    jsConsole.writeLine('Please enter number');
    return;
  }

  jsConsole.writeLine(str.toString().split("").reverse().join(""));
};