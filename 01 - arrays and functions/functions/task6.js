function compareNeighbors(element, leftNeighbor, rightNeighbor) {
  if (element) {
    if (leftNeighbor && rightNeighbor) {
      return {
        result: element > leftNeighbor && element > rightNeighbor,
        error: false,
      }
    } else if (leftNeighbor) {
      return {
        result: element > leftNeighbor,
        error: true,
      }
    } else if (rightNeighbor) {
      return {
        result: element > rightNeighbor,
        error: true,
      }
    }
  }
}

function twoNeighbors() {
  const str = jsConsole.read('#str1');
  const array = str.replace(/\s+/g,'').split(',').map(function (item) {
    return parseInt(item)
  });
  const pos = jsConsole.readInteger('#str2');
  const element = array[pos];
  const leftNeighbor = array[pos - 1];
  const rightNeighbor = array[pos + 1];
  const result = compareNeighbors(element, leftNeighbor, rightNeighbor);
  jsConsole.writeLine(result.result);
  if (result.error) {
    jsConsole.writeLine('The position can\'t be first or last in the sequence');
  }
}