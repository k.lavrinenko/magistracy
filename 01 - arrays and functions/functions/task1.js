// Функции

/*
  Напишите функцию, которая возвращает последнюю цифру заданного целого числа в виде английского слова
 */
const numbers = {
  '1': 'One',
  '2': 'Two',
  '3': 'Three',
  '4': 'Four',
  '5': 'Five',
  '6': 'Six',
  '7': 'Seven',
  '8': 'Eight',
  '9': 'Nine',
  '0': 'Zero',
};
function lastDigit() {
  const str = jsConsole.read('#str');
  jsConsole.writeLine(numbers[str[str.length - 1]])
};