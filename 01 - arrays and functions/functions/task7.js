function findPositionOfNumberWhichMoreThanNeighbors() {
  const str = jsConsole.read('#str');
  const array = str.replace(/\s+/g,'').split(',').map(function (item) {
    return parseInt(item)
  });
  let result = -1;
  for (let index = 0; index < array.length; index ++) {
    const element = array[index];
    const leftNeighbor = array[index - 1];
    const rightNeighbor = array[index + 1];
    const compareResult = compareNeighbors(element, leftNeighbor, rightNeighbor);
    if (compareResult.result) {
      result = index;
      break;
    }
  }
  jsConsole.writeLine(result);
}