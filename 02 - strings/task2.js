function checkBrackets () {
  const str = jsConsole.read('#str');
  let opened = 0;
  for (let index = 0; index < str.length; index ++) {
    const currentSymbol = str[index];
    if (currentSymbol === ')') {
      if (opened === 0) {
        jsConsole.writeLine(false);
        return;
      } else {
        opened -= 1;
      }
    }
    if (currentSymbol === '(') {
      opened += 1;
    }
  }
  jsConsole.writeLine(opened === 0);
}