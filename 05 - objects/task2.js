function findYoungestPerson() {
  var users  = [
      {
        firstName: 'Petr',
        lastName: 'Petrovih',
        age: 33,
      },
      {
        firstName: 'Oleg',
        lastName: 'Olegovich',
        age: 36,
      },
      {
        firstName: 'Ivan',
        lastName: 'Ivanovich',
        age: 55,
      },
  ];
  let result = users[0];
  users.forEach((user) => {
    if (user.age < result.age) {
      min = user.age;
    }
  })
  jsConsole.writeLine('The youngest person is:');
  jsConsole.writeLine(`${result.firstName} ${result.lastName} - ${result.age}`);
}